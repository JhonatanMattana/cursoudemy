// Armazenando um função dentro de uma variavel.
const imprimirSoma = function (a, b) {
    console.log(a + b);
}

imprimirSoma(2, 3);

// Armazenando uma função arrow em uma variavel.
// => Substitui o nome function.
const soma = (a, b) => {
    return a + b;
}
console.log(soma(5, 3));

// Retorno implícito
// Função de uma unica linha
const subtracao = (a, b) => a - b;
console.log(subtracao(10, 1));

const imprimir2 = a => console.log(a);
imprimir2('Teste...');