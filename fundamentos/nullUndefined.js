const a = {name: 'Teste'};
console.log(a);

const b = a;
console.log(b);

b.name = 'Opa';
console.log(a.name);

let c = 'kkk';
console.log(c);

let d = c;
console.log(d);
 
c = 'kakaka';
console.log(c, d);

let valor;// não inicializado
console.log(valor);

valor = null; // ausência de valor
console.log(valor);
//console.log(valor.toString()); // erro

const produto = {};
console.log(produto.preco);
console.log(produto);

produto.preco = 3.50;
console.log(produto);

produto.preco = undefined; // evite atribuir undefined
console.log(produto.preco);
console.log(produto);

//delete produto.preco;
console.log(produto);

console.log(!!produto.preco);

produto.preco = null;// sem preço
console.log(!!produto.preco);
console.log(produto);
